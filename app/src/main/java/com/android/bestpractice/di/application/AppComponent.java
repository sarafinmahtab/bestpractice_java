package com.android.bestpractice.di.application;


import android.content.Context;

import com.android.bestpractice.MyApplication;

import dagger.BindsInstance;
import dagger.Component;

// Scope annotation that the AppComponent uses
// Classes annotated with @ApplicationScope will have a unique instance in this Component
@AppScope
// Definition of a Dagger component that adds info from the different modules to the graph
@Component(
        modules = {DBModule.class}
)
public interface AppComponent {

    // Factory to create instances of the AppComponent
    @Component.Factory
    interface Factory {
        // With @BindsInstance, the Context passed in will be available in the graph
        AppComponent create(@BindsInstance Context context);
    }

    // Classes that can be injected by this Component
    void inject(MyApplication application);
}
