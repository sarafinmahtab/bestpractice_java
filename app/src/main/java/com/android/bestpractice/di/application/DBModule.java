package com.android.bestpractice.di.application;

import android.content.Context;
import android.content.SharedPreferences;

import com.android.bestpractice.data.AppConstants;

import dagger.Module;
import dagger.Provides;

@Module
class DBModule {

    @Provides
    @AppScope
    public SharedPreferences getSharedPreferences(Context context) {
        return context.getSharedPreferences(AppConstants.SHARED_PREF, Context.MODE_PRIVATE);
    }
}
