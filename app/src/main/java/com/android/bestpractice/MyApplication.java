package com.android.bestpractice;

import android.app.Application;

import androidx.annotation.UiThread;

import com.android.bestpractice.di.application.AppComponent;
import com.android.bestpractice.di.application.DaggerAppComponent;

public class MyApplication extends Application {

    private AppComponent appComponent;

    @UiThread
    public AppComponent getAppComponent() {

        if (appComponent == null) {
            appComponent = DaggerAppComponent.factory()
                    .create(getApplicationContext());
        }

        return appComponent;
    }
}
